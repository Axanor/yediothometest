package com.pavel.yittest.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pavel.yittest.data.PicItem


@Dao
interface PicItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: PicItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(entry: List<PicItem>)

    @Query("SELECT * FROM PicItem")
    fun getAllPicsLiveData(): LiveData<List<PicItem>>

    @Query("DELETE FROM PicItem")
    fun delAll()


}