package com.pavel.yittest.db

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

import com.pavel.yittest.data.PicItem


@Database(entities = [PicItem::class], version = 2, exportSchema = false)
abstract class AppDatabase:RoomDatabase() {

    abstract fun picItemDAO(): PicItemDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Application): AppDatabase? {
            if (INSTANCE == null){
                synchronized(AppDatabase::class){
                    INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "appDB")
                        .allowMainThreadQueries().fallbackToDestructiveMigration().build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }

}