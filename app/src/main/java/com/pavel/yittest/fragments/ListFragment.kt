package com.pavel.yittest.fragments

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.pavel.yittest.ImagesViewModel
import com.pavel.yittest.PicClickListener
import com.pavel.yittest.R
import com.pavel.yittest.adapters.PicItemsAdapter
import com.pavel.yittest.data.PicItem
import com.pavel.yittest.databinding.FragmentListBinding
import com.pavel.yittest.utils.SpUtil
import com.pavel.yittest.utils.Utils
import com.pavel.yittest.view_cust.ItemDecoration


class ListFragment : Fragment() {

    private lateinit var binding: FragmentListBinding
    private lateinit var viewModel: ImagesViewModel
    private lateinit var picItemsAdapter: PicItemsAdapter

    private val observer = Observer<List<PicItem>> { items ->
        binding.showLoading = false
        val lastPos = picItemsAdapter.items.lastIndex
        //insert only items that are not in the list already
        items.forEachIndexed{index, item ->
            if(index>lastPos){
                Log.d("log","adding")
                picItemsAdapter.items.add(item)
                picItemsAdapter.notifyItemInserted(lastPos+index)
            }
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater)
        viewModel = ViewModelProvider(requireActivity()).get(ImagesViewModel::class.java)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeUi()
    }


    private fun subscribeUi() {
        val picClickListener = object : PicClickListener {
            override fun onPicClicked(pic: PicItem) {
                openPic(pic)
            }
        }
        with(binding) {
            val gridLayoutManager = GridLayoutManager(requireContext(),4 )
            recyclerView.layoutManager = gridLayoutManager
            picItemsAdapter = PicItemsAdapter(mutableListOf<PicItem>(), picClickListener)
            adapter = picItemsAdapter

            //items spacing
            recyclerView.addItemDecoration(ItemDecoration(Utils.dp2px(4, requireContext())))

            //probably had to use pagination here, but it looks an overkill for
            // this task
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val pos = gridLayoutManager.findLastCompletelyVisibleItemPosition()
                    if (pos >= picItemsAdapter.itemCount-4){
                        viewModel.loadNextPage()
                    }
                }
            })

            //new search keyword
            searchBtn.setOnClickListener() {
                hideKeyBoard(binding.searchBtn)
                if (editTextSearch.text.toString().trim() != "") {
                    viewModel.performSearch(editTextSearch.text.toString().trim())
                    picItemsAdapter.items.clear()
                    picItemsAdapter.notifyDataSetChanged()
                    showLoading = true
                } else {
                    showErrorToast(getString(R.string.keyword_pls))
                }
            }

            //show last keyword on app start
            editTextSearch.setText(SpUtil.getString(SpUtil.KEYWORD_KEY, ""))
        }
        viewModel.getLiveData().observe(viewLifecycleOwner, observer)
    }


    fun hideKeyBoard(view: View) {
        try {
            val imm =
                activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showErrorToast(text:String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
    }

    fun openPic(item: PicItem) {
        findNavController().navigate(ListFragmentDirections.actionListFragment2ToPicFragment(item))
    }


}