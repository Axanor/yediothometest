package com.pavel.yittest.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.omega_r.libs.omegaintentbuilder.OmegaIntentBuilder
import com.omega_r.libs.omegaintentbuilder.downloader.DownloadCallback
import com.omega_r.libs.omegaintentbuilder.handlers.ContextIntentHandler
import com.omega_r.libs.omegaintentbuilder.types.MimeTypes
import com.pavel.yittest.databinding.FragmentPicBinding


class PicFragment:Fragment() {
    val args:PicFragmentArgs by navArgs()
    private lateinit var binding: FragmentPicBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPicBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeUi()

    }

    private fun subscribeUi() {
        binding.url = args.item.picUrl
        binding.imageViewShare.setOnClickListener {
            shareImage()
        }
    }

    private fun shareImage() {
        OmegaIntentBuilder.from(requireContext())
            .share()
            .subject("Image")
            .fileUrlWithMimeType(
                args.item.picUrl,
                MimeTypes.IMAGE_PNG
            )
            .download(object : DownloadCallback {
                override fun onDownloaded(success: Boolean, contextIntentHandler: ContextIntentHandler) {
                    contextIntentHandler.startActivity()
                }
            })
    }

}