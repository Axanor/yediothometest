package com.pavel.yittest.utils

import com.pavel.yittest.App

object SpUtil {
    const val PREFS_FILENAME = "com.pavel.yittest.prefs"
    const val KEYWORD_KEY = "com.pavel.yittest_keyword"

    @JvmStatic
    fun storeString(key: String, text: String) {
        val editor = App.instance.getSharedPreferences(PREFS_FILENAME, 0)!!.edit()
        editor.putString(key, text)
        editor.apply()
    }

    @JvmStatic
    fun getString(key: String, def: String): String {
        val text = App.instance.getSharedPreferences(PREFS_FILENAME, 0).getString(key, def)?:""
        return text
    }


}
