package com.pavel.yittest.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.pavel.yittest.R
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter("visibleGone")
fun View.visibleGone(visible: Boolean){
    visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun loadImage(view: AppCompatImageView, url: String?) {
    view.setImageDrawable(null)
    if (!url.isNullOrEmpty()) {
        Glide.with(view.context)
            .load(url)
            .placeholder(R.drawable.ic_baseline_downloading_24)
            .error(R.drawable.placeholder)
            .fitCenter()
            .into(view)
    }
}
