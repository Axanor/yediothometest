package com.pavel.yittest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.pavel.yittest.PicClickListener
import com.pavel.yittest.R
import com.pavel.yittest.data.PicItem
import com.pavel.yittest.databinding.ItemListBinding


class PicItemsAdapter(var items: MutableList<PicItem>,val listener: PicClickListener) :
    RecyclerView.Adapter<PicItemsAdapter.PicItemsViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PicItemsViewHolder {
        val dataBinding: ItemListBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_list,
                parent,
                false
            )
        return PicItemsViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: PicItemsViewHolder, position: Int) {
        holder.dataBinding.item = items[position]
        holder.dataBinding.listener = listener

    }

    class PicItemsViewHolder (val dataBinding: ItemListBinding):RecyclerView.ViewHolder(dataBinding.root)
}