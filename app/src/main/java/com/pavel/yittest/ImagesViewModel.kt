package com.pavel.yittest

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.pavel.yittest.data.PicItem
import com.pavel.yittest.repository.ImagesRepo


class ImagesViewModel : ViewModel() {

    companion object {
        private val imagesRepo = ImagesRepo()
    }

    fun performSearch(keyWord: String) {
        imagesRepo.performSearch(keyWord)
    }


    fun loadNextPage() {
        imagesRepo.loadNextPage()
    }

    fun getLiveData(): LiveData<List<PicItem>> {
        return imagesRepo.imagesLivedata
    }


}