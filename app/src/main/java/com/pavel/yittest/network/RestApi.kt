package com.pavel.yittest.network

import retrofit2.Call
import retrofit2.http.*


interface RestApi {

    @Headers("Content-Type: application/json")
    @GET()
    fun loadImages(@Url urlQueryPart:String): Call<ImagesResponse>

}