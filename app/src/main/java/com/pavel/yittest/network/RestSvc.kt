package com.pavel.yittest.network

import com.pavel.yittest.repository.ImagesRepo

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RestSvc {

    fun loadImages(imagesRepo: ImagesRepo, queryUrl:String) {
        val retrofit = ServiceBuilder.buildService(RestApi::class.java)
        retrofit.loadImages(queryUrl).enqueue(
            object : Callback<ImagesResponse>{
                override fun onResponse(
                    call: Call<ImagesResponse>,
                    response: Response<ImagesResponse>
                ) {
                    if (response.body()!=null){
                        imagesRepo.processResponse(response.body()!!.hits)
                    } else {
                        //TODO:show error message

                    }
                }

                override fun onFailure(call: Call<ImagesResponse>, t: Throwable) {
                    //TODO:show error message

                }
            }
        )
    }

    //fun loadImages2(imagesRepo: ImagesRepo, queryUrl:String, page:Int) :
}