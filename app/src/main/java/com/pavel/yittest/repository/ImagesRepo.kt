package com.pavel.yittest.repository

import android.util.Log
import androidx.lifecycle.LiveData

import com.pavel.yittest.App
import com.pavel.yittest.Constants
import com.pavel.yittest.data.PicItem
import com.pavel.yittest.db.AppDatabase

import com.pavel.yittest.network.RestSvc
import com.pavel.yittest.utils.SpUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ImagesRepo {

    private val db = AppDatabase.getAppDataBase(App.instance)!!
    val imagesLivedata: LiveData<List<PicItem>> = db.picItemDAO().getAllPicsLiveData()
    var currentKeyWord = SpUtil.getString(SpUtil.KEYWORD_KEY, "")
    var currentPage = 1


    fun processResponse(response: List<PicItem>) {
        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                db.picItemDAO().insertAll(response)
            }
        }
    }

    fun performSearch(keyWord: String){
        db.picItemDAO().delAll()
        currentKeyWord = keyWord
        currentPage = 1
        SpUtil.storeString(SpUtil.KEYWORD_KEY, keyWord)
        load()
    }

    fun loadNextPage(){
        currentPage+=1
        load()
    }

    private fun load() {
        val queryString = "?q=$currentKeyWord&key=${Constants.API_KEY}&page=$currentPage&per_page=${Constants.PER_PAGE}"
        Log.d("query","queryString = $queryString")
        RestSvc().loadImages(this,queryString)
    }
}


