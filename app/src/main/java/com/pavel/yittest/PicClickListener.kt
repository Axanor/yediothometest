package com.pavel.yittest

import com.pavel.yittest.data.PicItem


interface PicClickListener {

    fun onPicClicked(pic: PicItem)

}