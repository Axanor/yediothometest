package com.pavel.yittest.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
data class PicItem(@SerializedName("largeImageURL")val picUrl:String,
                   @SerializedName("previewURL") val smallPicUrl:String,
                   @PrimaryKey val id:Long): Parcelable
